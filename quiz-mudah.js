// select all elements
const start = document.getElementById("start");
const quiz = document.getElementById("quiz");
const question = document.getElementById("question");
const qImg = document.getElementById("qImg");
const choiceA = document.getElementById("A");
const choiceB = document.getElementById("B");
const choiceC = document.getElementById("C");
const counter = document.getElementById("counter");
const timeGauge = document.getElementById("timeGauge");
const progress = document.getElementById("progress");
const scoreDiv = document.getElementById("scoreContainer");

// buat pertanyaan 
let questions = [
    {
        question : "She _____ her dog everyday",
        imgSrc : "img/html.png",
        choiceA : "To feed",
        choiceB : "Feed",
        choiceC : "Feeds",
        correct : "C"
    },{
        question : "I ____ always ____ to the dentist",
        imgSrc : "img/css.png",
        choiceA : "Do not, go",
        choiceB : "Does not, go",
        choiceC : "Do not, went",
        correct : "A"
    },{
        question : "When __ you ____ a shower?",
        imgSrc : "img/js.png",
        choiceA : "Do, took",
        choiceB : "Do, take",
        choiceC : "Do, taken",
        correct : "B"
    },{
		question : "Amir and Udin …….. discussing the material now.",
        imgSrc : "img/css.png",
        choiceA : "Is",
        choiceB : "Are",
        choiceC : "Was",
        correct : "B"
	},{
		question : "Listen! She’s…….. a beautiful song.",
        imgSrc : "img/css.png",
        choiceA : "Sing",
        choiceB : "Singing",
        choiceC : "To Sing",
        correct : "B"
	},{
		question : "…… a noise just then?",
        imgSrc : "img/css.png",
        choiceA : "Did you hear?",
        choiceB : "Do you hear?",
        choiceC : "Have you heard?",
        correct : "C"
	},{
		question : "They are singing and dancing because their uncle……...",
        imgSrc : "img/css.png",
        choiceA : "is arriving",
        choiceB : "has arrived",
        choiceC : "arrives",
        correct : "B"
	},{
		question : "You’ve finished the test. The negative form of this sentence is….",
        imgSrc : "img/css.png",
        choiceA : "You don’t finish the test.",
        choiceB : "You aren’t finishing the test.",
        choiceC : "You haven’t finished the test yet.",
        correct : "C"
	},{
		question : "Today, it ......raining for two hours",
        imgSrc : "img/css.png",
        choiceA : "Has",
        choiceB : "Have",
        choiceC : "Have Been",
        correct : "C"
	},{
		question : "I .........learning english for four years",
        imgSrc : "img/css.png",
        choiceA : "Has",
        choiceB : "Have Been",
        choiceC : "Has Been",
        correct : "B"
	},{
		question : ". I .......you flowers when you was in hospital two days ago",
        imgSrc : "img/css.png",
        choiceA : "Bring",
        choiceB : "Brought",
        choiceC : "Bringing",
        correct : "B"
	},{
		question : "She .........watching TV for four hours ",
        imgSrc : "img/css.png",
        choiceA : "Has",
        choiceB : "Have",
        choiceC : "Have Been",
        correct : "C"
	},{
		question : "The thief ......through the window to steal your phone ",
        imgSrc : "img/css.png",
        choiceA : "Walk",
        choiceB : "Walking",
        choiceC : "Walked",
        correct : "C"
    },{
        question : "The plane landed............. ",
        imgSrc : "img/css.png",
        choiceA : "an hour ago",
        choiceB : "Tommorow",
        choiceC : "Yesterday",
        correct : "A"
    }
];

// membuat variabel

const lastQuestion = questions.length - 1;
let runningQuestion = 0;
let count = 0;
const questionTime = 10; // 10 detik
const gaugeWidth = 150; // --> 150px
const gaugeUnit = gaugeWidth / questionTime;
let TIMER;
let score = 0;

// render pertanyaan
function renderQuestion(){
    let q = questions[runningQuestion];
    
    question.innerHTML = "<p>"+ q.question +"</p>";
    qImg.innerHTML = "<img src="+ q.imgSrc +">";
    choiceA.innerHTML = q.choiceA;
    choiceB.innerHTML = q.choiceB;
    choiceC.innerHTML = q.choiceC;
}

start.addEventListener("click",startQuiz);

// mulai quiz
function startQuiz(){
    start.style.display = "none";
    renderQuestion();
    quiz.style.display = "block";
    renderProgress();
    renderCounter();
    TIMER = setInterval(renderCounter,1000); // 1000ms = 1s
}

// render progress
function renderProgress(){
    for(let qIndex = 0; qIndex <= lastQuestion; qIndex++){
        progress.innerHTML += "<div class='prog' id="+ qIndex +"></div>";
    }
}

// counter render

function renderCounter(){
    if(count <= questionTime){
        counter.innerHTML = count;
        timeGauge.style.width = count * gaugeUnit + "px";
        count++
    }else{
        count = 0;
        // ngeganti warna progress jadi merah
        answerIsWrong();
        if(runningQuestion < lastQuestion){
            runningQuestion++;
            renderQuestion();
        }else{
            // end the quiz and show the score
            clearInterval(TIMER);
            scoreRender();
        }
    }
}

// cek jawaban

function checkAnswer(answer){
    if( answer == questions[runningQuestion].correct){
        // jawaban benar
        score++;
        // progress berubah warna hijau
        answerIsCorrect();
    }else{
        // jawaban salah
        // progress berubah warna merah
        answerIsWrong();
    }
    count = 0;
    if(runningQuestion < lastQuestion){
        runningQuestion++;
        renderQuestion();
    }else{
        // akhir dari quiz dan menunjukan score
        clearInterval(TIMER);
        scoreRender();
    }
}

// jawaban benar
function answerIsCorrect(){
    document.getElementById(runningQuestion).style.backgroundColor = "#0f0";
}

// jawaban salah
function answerIsWrong(){
    document.getElementById(runningQuestion).style.backgroundColor = "#f00";
}

// score render
function scoreRender(){
    scoreDiv.style.display = "block";
    
    // kalkulasi persenan berdasarkan jawaban pengguna 
    const scorePerCent = Math.round(100 * score/questions.length);
    
    // memilih gambar sesuai hasil jawaban persenan
    let img = (scorePerCent >= 80) ? "img/5.png" :
              (scorePerCent >= 60) ? "img/4.png" :
              (scorePerCent >= 40) ? "img/3.png" :
              (scorePerCent >= 20) ? "img/2.png" :
              "img/1.png";
    
    scoreDiv.innerHTML = "<img src="+ img +">";
    scoreDiv.innerHTML += "<p>"+ scorePerCent +"%</p>";
}





















