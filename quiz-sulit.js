// select all elements
const start = document.getElementById("start");
const quiz = document.getElementById("quiz");
const question = document.getElementById("question");
const qImg = document.getElementById("qImg");
const choiceA = document.getElementById("A");
const choiceB = document.getElementById("B");
const choiceC = document.getElementById("C");
const counter = document.getElementById("counter");
const timeGauge = document.getElementById("timeGauge");
const progress = document.getElementById("progress");
const scoreDiv = document.getElementById("scoreContainer");

// buat pertanyaan 
let questions = [
    {
        question : "We will have ......married on this end of the year",
        imgSrc : "img/html.png",
        choiceA : "Been",
        choiceB : "Be",
        choiceC : "Being",
        correct : "A"
    },{
        question : "The pizza delivery will have .....arrived here for 30 minutes.",
        imgSrc : "img/css.png",
        choiceA : "Be",
        choiceB : "Been",
        choiceC : "Being",
        correct : "B"
    },{
        question : "I will have been ......my bedroom when I finish my homework.",
        imgSrc : "img/js.png",
        choiceA : "Clean",
        choiceB : "Cleaning",
        choiceC : "Cleaned",
        correct : "C"
    },{
		question : "She will have .......watching TV when she's home ",
        imgSrc : "img/css.png",
        choiceA : "Be",
        choiceB : "Being",
        choiceC : "Been",
        correct : "C"
	},{
		question : "I will have been ......the car in the whole tomorrow night",
        imgSrc : "img/css.png",
        choiceA : "Drove",
        choiceB : "Drive",
        choiceC : "Driving",
        correct : "C"
	},{
        question : "I will have been ........for you tomorrow night",
        imgSrc : "img/html.png",
        choiceA : "Waiting",
        choiceB : "Wait",
        choiceC : "Waited",
        correct : "A"
    },{
        question : "I would .......you If I knew that you were sick yesterday. ",
        imgSrc : "img/css.png",
        choiceA : "Visit",
        choiceB : "Visiting",
        choiceC : "Visited",
        correct : "A"
    },{
        question : "I would ..........the sarah sechan show if I knew that the guest star was Justin Bieber.",
        imgSrc : "img/js.png",
        choiceA : "Watch",
        choiceB : "Watched",
        choiceC : "Watching",
        correct : "A"
    },{
		question : "My father would ....... more grapes, if he knew that my sister likes it a lot.",
        imgSrc : "img/css.png",
        choiceA : "Buy",
        choiceB : "Bought",
        choiceC : "Buying",
        correct : "A"
	},{
		question : "My mother would have ... the poisoned food if I hadn't told her",
        imgSrc : "img/css.png",
        choiceA : "Eat",
        choiceB : "Eating",
        choiceC : "Eaten",
        correct : "C"
	},{
        question : "She would have ..... this painting a lot, If she had been here",
        imgSrc : "img/html.png",
        choiceA : "Liked",
        choiceB : "Like",
        choiceC : "Lick",
        correct : "A"
    },{
        question : "My cat would have .... birth to her children if she hadn't died when pregnant.",
        imgSrc : "img/css.png",
        choiceA : "Give",
        choiceB : "Gave",
        choiceC : "Given",
        correct : "B"
    },{
        question : "I would ...... been traveling to South Korea last month if my passport didn't lost",
        imgSrc : "img/js.png",
        choiceA : "Haved",
        choiceB : "Has",
        choiceC : "Have",
        correct : "C"
    },{
		question : "She would have been .... for taxi to come when she came back from school ",
        imgSrc : "img/css.png",
        choiceA : "Wait",
        choiceB : "Waited",
        choiceC : "Waiting",
        correct : "C"
	},{
		question : "I would not have been ....... instant noodle if I knew the side effects of it",
        imgSrc : "img/css.png",
        choiceA : "Eat",
        choiceB : "Eaten",
        choiceC : "Eating",
        correct : "C"
	}
];

// membuat variabel

const lastQuestion = questions.length - 1;
let runningQuestion = 0;
let count = 0;
const questionTime = 10; // 10 detik
const gaugeWidth = 150; // --> 150px
const gaugeUnit = gaugeWidth / questionTime;
let TIMER;
let score = 0;

// render pertanyaan
function renderQuestion(){
    let q = questions[runningQuestion];
    
    question.innerHTML = "<p>"+ q.question +"</p>";
    qImg.innerHTML = "<img src="+ q.imgSrc +">";
    choiceA.innerHTML = q.choiceA;
    choiceB.innerHTML = q.choiceB;
    choiceC.innerHTML = q.choiceC;
}

start.addEventListener("click",startQuiz);

// mulai quiz
function startQuiz(){
    start.style.display = "none";
    renderQuestion();
    quiz.style.display = "block";
    renderProgress();
    renderCounter();
    TIMER = setInterval(renderCounter,1000); // 1000ms = 1s
}

// render progress
function renderProgress(){
    for(let qIndex = 0; qIndex <= lastQuestion; qIndex++){
        progress.innerHTML += "<div class='prog' id="+ qIndex +"></div>";
    }
}

// counter render

function renderCounter(){
    if(count <= questionTime){
        counter.innerHTML = count;
        timeGauge.style.width = count * gaugeUnit + "px";
        count++
    }else{
        count = 0;
        // ngeganti warna progress jadi merah
        answerIsWrong();
        if(runningQuestion < lastQuestion){
            runningQuestion++;
            renderQuestion();
        }else{
            // end the quiz and show the score
            clearInterval(TIMER);
            scoreRender();
        }
    }
}

// cek jawaban

function checkAnswer(answer){
    if( answer == questions[runningQuestion].correct){
        // jawaban benar
        score++;
        // progress berubah warna hijau
        answerIsCorrect();
    }else{
        // jawaban salah
        // progress berubah warna merah
        answerIsWrong();
    }
    count = 0;
    if(runningQuestion < lastQuestion){
        runningQuestion++;
        renderQuestion();
    }else{
        // akhir dari quiz dan menunjukan score
        clearInterval(TIMER);
        scoreRender();
    }
}

// jawaban benar
function answerIsCorrect(){
    document.getElementById(runningQuestion).style.backgroundColor = "#0f0";
}

// jawaban salah
function answerIsWrong(){
    document.getElementById(runningQuestion).style.backgroundColor = "#f00";
}

// score render
function scoreRender(){
    scoreDiv.style.display = "block";
    
    // kalkulasi persenan berdasarkan jawaban pengguna 
    const scorePerCent = Math.round(100 * score/questions.length);
    
    // memilih gambar sesuai hasil jawaban persenan
    let img = (scorePerCent >= 80) ? "img/5.png" :
              (scorePerCent >= 60) ? "img/4.png" :
              (scorePerCent >= 40) ? "img/3.png" :
              (scorePerCent >= 20) ? "img/2.png" :
              "img/1.png";
    
    scoreDiv.innerHTML = "<img src="+ img +">";
    scoreDiv.innerHTML += "<p>"+ scorePerCent +"%</p>";
}





















