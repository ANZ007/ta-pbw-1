// select all elements
const start = document.getElementById("start");
const quiz = document.getElementById("quiz");
const question = document.getElementById("question");
const qImg = document.getElementById("qImg");
const choiceA = document.getElementById("A");
const choiceB = document.getElementById("B");
const choiceC = document.getElementById("C");
const counter = document.getElementById("counter");
const timeGauge = document.getElementById("timeGauge");
const progress = document.getElementById("progress");
const scoreDiv = document.getElementById("scoreContainer");

// buat pertanyaan 
let questions = [
    {
        question : "I .......when you came last night",
        imgSrc : "img/html.png",
        choiceA : "Was Crying",
        choiceB : "Ware Crying",
        choiceC : "Ware Crying",
        correct : "A"
    },{
        question : "I .....listening to the music when you knocked at my door ",
        imgSrc : "img/css.png",
        choiceA : "Am",
        choiceB : "Was",
        choiceC : "Were",
        correct : "B"
    },{
        question : "That man .....smoking when he carried the baby",
        imgSrc : "img/js.png",
        choiceA : "Am",
        choiceB : "Were",
        choiceC : "Was",
        correct : "C"
    },{
		question : "We .....playing football when the sun goes down ",
        imgSrc : "img/css.png",
        choiceA : "Ware",
        choiceB : "Was",
        choiceC : "Were",
        correct : "C"
	},{
		question : "We .....playing football when the sun goes down ",
        imgSrc : "img/css.png",
        choiceA : "Ware",
        choiceB : "Was",
        choiceC : "Were",
        correct : "C"
	},{
        question : "The audience aplauded after the artist ....given a very baeutiful performance.",
        imgSrc : "img/html.png",
        choiceA : "Had",
        choiceB : "Have",
        choiceC : "Was",
        correct : "A"
    },{
        question : " ......you lived in Bandung before you lived here ?",
        imgSrc : "img/css.png",
        choiceA : "Had",
        choiceB : "Have",
        choiceC : "Were",
        correct : "A"
    },{
        question : "How long ....you used the Internet when I went out?",
        imgSrc : "img/js.png",
        choiceA : "Had",
        choiceB : "Have",
        choiceC : "Were",
        correct : "A"
    },{
		question : "My sister had been .......TV since I went to school.",
        imgSrc : "img/css.png",
        choiceA : "Watching",
        choiceB : "Watched",
        choiceC : "Watch",
        correct : "A"
	},{
		question : "My baby had been ........since I went to the grocery store. ",
        imgSrc : "img/css.png",
        choiceA : "Sleeped",
        choiceB : "Sleepy",
        choiceC : "Sleeping",
        correct : "C"
	},{
        question : "My father had been ......in the lake more than two hours before he went home. ",
        imgSrc : "img/html.png",
        choiceA : "Fishing",
        choiceB : "Fished",
        choiceC : "Fises",
        correct : "A"
    },{
        question : "You had been ...........that novel for a week before you bought the new one.",
        imgSrc : "img/css.png",
        choiceA : "Read",
        choiceB : "Reading",
        choiceC : "Readed",
        correct : "B"
    },{
        question : "King Salman and his family will be ........Bali for a week. ",
        imgSrc : "img/js.png",
        choiceA : "Visit",
        choiceB : "Visited",
        choiceC : "Visiting",
        correct : "C"
    },{
		question : "I will be ..........until 5 a.m, please don't wake me up. ",
        imgSrc : "img/css.png",
        choiceA : "Sleepy",
        choiceB : "Slap",
        choiceC : "Sleeping",
        correct : "C"
	},{
		question : "My mother will be.......... as a labor in the Ultra Milk factory for 2 years. ",
        imgSrc : "img/css.png",
        choiceA : "Work",
        choiceB : "Worked",
        choiceC : "Working",
        correct : "C"
	}
];

// membuat variabel

const lastQuestion = questions.length - 1;
let runningQuestion = 0;
let count = 0;
const questionTime = 10; // 10 detik
const gaugeWidth = 150; // --> 150px
const gaugeUnit = gaugeWidth / questionTime;
let TIMER;
let score = 0;

// render pertanyaan
function renderQuestion(){
    let q = questions[runningQuestion];
    
    question.innerHTML = "<p>"+ q.question +"</p>";
    qImg.innerHTML = "<img src="+ q.imgSrc +">";
    choiceA.innerHTML = q.choiceA;
    choiceB.innerHTML = q.choiceB;
    choiceC.innerHTML = q.choiceC;
}

start.addEventListener("click",startQuiz);

// mulai quiz
function startQuiz(){
    start.style.display = "none";
    renderQuestion();
    quiz.style.display = "block";
    renderProgress();
    renderCounter();
    TIMER = setInterval(renderCounter,1000); // 1000ms = 1s
}

// render progress
function renderProgress(){
    for(let qIndex = 0; qIndex <= lastQuestion; qIndex++){
        progress.innerHTML += "<div class='prog' id="+ qIndex +"></div>";
    }
}

// counter render

function renderCounter(){
    if(count <= questionTime){
        counter.innerHTML = count;
        timeGauge.style.width = count * gaugeUnit + "px";
        count++
    }else{
        count = 0;
        // ngeganti warna progress jadi merah
        answerIsWrong();
        if(runningQuestion < lastQuestion){
            runningQuestion++;
            renderQuestion();
        }else{
            // end the quiz and show the score
            clearInterval(TIMER);
            scoreRender();
        }
    }
}

// cek jawaban

function checkAnswer(answer){
    if( answer == questions[runningQuestion].correct){
        // jawaban benar
        score++;
        // progress berubah warna hijau
        answerIsCorrect();
    }else{
        // jawaban salah
        // progress berubah warna merah
        answerIsWrong();
    }
    count = 0;
    if(runningQuestion < lastQuestion){
        runningQuestion++;
        renderQuestion();
    }else{
        // akhir dari quiz dan menunjukan score
        clearInterval(TIMER);
        scoreRender();
    }
}

// jawaban benar
function answerIsCorrect(){
    document.getElementById(runningQuestion).style.backgroundColor = "#0f0";
}

// jawaban salah
function answerIsWrong(){
    document.getElementById(runningQuestion).style.backgroundColor = "#f00";
}

// score render
function scoreRender(){
    scoreDiv.style.display = "block";
    
    // kalkulasi persenan berdasarkan jawaban pengguna 
    const scorePerCent = Math.round(100 * score/questions.length);
    
    // memilih gambar sesuai hasil jawaban persenan
    let img = (scorePerCent >= 80) ? "img/5.png" :
              (scorePerCent >= 60) ? "img/4.png" :
              (scorePerCent >= 40) ? "img/3.png" :
              (scorePerCent >= 20) ? "img/2.png" :
              "img/1.png";
    
    scoreDiv.innerHTML = "<img src="+ img +">";
    scoreDiv.innerHTML += "<p>"+ scorePerCent +"%</p>";
}





















